//
//  UIImage+Extensions.swift
//  传智微博
//
//  Created by 刘凡 on 2016/7/5.
//  Copyright © 2016年 itcast. All rights reserved.
//

import UIKit

extension UIImage {
    
    /// 创建头像图像
    ///
    /// - parameter size:      尺寸
    /// - parameter backColor: 背景颜色
    ///
    /// - returns: 裁切后的图像
    func cz_avatarImage(size: CGSize?, backColor: UIColor = UIColor.white, lineColor: UIColor = UIColor.lightGray) -> UIImage? {
        //判断size
        var size = size
        if size == nil || size?.width == 0 {
            size = CGSize(width: 34, height: 34)
        }
        //设置rect
        let rect = CGRect(origin: CGPoint(), size: size!)
        //图像上下文
        //size:绘图的尺寸，opaque=true表示不透明，scale = 0会选择当前设备的屏幕分辨率
        UIGraphicsBeginImageContextWithOptions(rect.size, true, 0)
        //填充背景
        backColor.setFill()
        //
        UIRectFill(rect)
        //路径：圆
        let path = UIBezierPath(ovalIn: rect)
        path.addClip()
        
        draw(in: rect)
        //边框
        let ovalPath = UIBezierPath(ovalIn: rect)
        ovalPath.lineWidth = 2
        lineColor.setStroke()
        ovalPath.stroke()
        //取得结果
        let result = UIGraphicsGetImageFromCurrentImageContext()
        //关闭上下文
        UIGraphicsEndImageContext()
        //返回结果
        return result
        
        
        /*画圆角矩形*/
//        float fw = 180;
//        float fh = 280;
//        
//        CGContextMoveToPoint(context, fw, fh-20);  // 开始坐标右边开始
//        CGContextAddArcToPoint(context, fw, fh, fw-20, fh, 10);  // 右下角角度
//        CGContextAddArcToPoint(context, 120, fh, 120, fh-20, 10); // 左下角角度
//        CGContextAddArcToPoint(context, 120, 250, fw-20, 250, 10); // 左上角
//        CGContextAddArcToPoint(context, fw, 250, fw, fh-20, 10); // 右上角
//        CGContextClosePath(context);
//        CGContextDrawPath(context, kCGPathFillStroke); //根据坐标绘制路径
    }
    
    /// 生成指定大小的不透明图象
    ///
    /// - parameter size:      尺寸
    /// - parameter backColor: 背景颜色
    ///
    /// - returns: 图像
    func cz_image(size: CGSize? = nil, backColor: UIColor = UIColor.white) -> UIImage? {
        //判断size
        var size = size
        if size == nil {
            size = self.size
        }
        //设置rect
        let rect = CGRect(origin: CGPoint(), size: size!)
        //开启imageContext
        UIGraphicsBeginImageContextWithOptions(rect.size, true, 0)
        //设置背景色
        backColor.setFill()
        UIRectFill(rect)
        //绘图
        draw(in: rect)
        //取得结果
        let result = UIGraphicsGetImageFromCurrentImageContext()
        //关闭上下文
        UIGraphicsEndImageContext()
        //返回结果
        return result
    }
}
